<?php

/**
 * cm_MemberGoogleMaps
 * Extension for Contao Open Source CMS (contao.org)
 *
 * @copyright Christian Muenster 2014 
 * @package cm_MemberGoogleMaps
 * @author  Christian Münster (aka ChrMue)
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

//die("xxx");                
if(class_exists('\\ChrMue\\cm_MemberGoogleMaps\\UpgradeHandler'))
{
	\ChrMue\cm_MemberGoogleMaps\UpgradeHandler::run();
}