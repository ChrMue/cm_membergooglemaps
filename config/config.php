<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2009-2020
 * @author     Christian Muenster
 * @package    cm_MemberMaps
 * @license    LGPL 
 */


/**
 * Back end wizards
 */
$GLOBALS['BE_FFL']['cm_SortWizard'] = 'ChrMue\cm_MemberGoogleMaps\cm_SortWizard';
$GLOBALS['BE_FFL']['cm_ListWizard'] = 'ChrMue\cm_MemberGoogleMaps\cm_ListWizard';


/**
 * Front end modules
 */
$GLOBALS['FE_MOD']['user']['cm_memberfinder'] = 'cm_MemberGoogleMaps\ModuleMemberFinder';
//$GLOBALS['FE_MOD']['user']['cm_membergooglemaps'] = 'ModuleCM_MemberGoogleMaps';
$GLOBALS['FE_MOD']['user']['cm_membergooglemaps'] = 'cm_MemberGoogleMaps\ModuleMemberGoogleMapsTags';

$GLOBALS['FE_MOD']['user']['cm_membergooglemapsList'] = 'cm_MemberGoogleMaps\ModuleMemberGoogleMapsList';
$GLOBALS['FE_MOD']['user']['cm_membergooglemapsReader'] = 'cm_MemberGoogleMaps\ModuleMemberGoogleMapsReader';


$GLOBALS['FE_MOD']['user']['cm_membergooglemapspos'] = 'cm_MemberGoogleMaps\ModuleMemberGoogleMapsPos';

if (TL_MODE == 'FE')
{
	$GLOBALS['TL_CSS'][] = 'system/modules/cm_membergooglemaps/assets/cm_member.css';
    //$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_membergooglemaps/assets/markerclusterer.js'; 
	//$GLOBALS['TL_JAVASCRIPT'][] = 'https://rawgit.com/googlemaps/js-marker-clusterer/gh-pages/src/markerclusterer.js'; 
}
if (TL_MODE == 'BE')
{
	$GLOBALS['TL_CSS'][] = 'system/modules/cm_membergooglemaps/assets/cm_memberBE.css'; 
}

//$GLOBALS['TL_HOOKS']['createNewUser'][] = array('cm_MemberGoogleMaps\tl_cm_memberlist', 'createNewUser');
$GLOBALS['TL_HOOKS']['createNewUser'][] = array('tl_cm_memberlist', 'createNewUser');

$GLOBALS['TL_CRON']['hourly'][] = array('cm_MemberGoogleMaps\memberGM', 'cronGeoRouting');

$GLOBALS['BE_MOD']['accounts']['member']['updCoords'] = array('cm_MemberGoogleMaps\memberGM', 'geoRouting'); 
$GLOBALS['BE_MOD']['accounts']['member']['stylesheet'] = 'system/modules/cm_membergooglemaps/assets/cm_memberBE.css'; 
$GLOBALS['BE_MOD']['accounts']['member']['tables'][]= 'tl_content';

if (version_compare(VERSION . '.' . BUILD, '2.9.9', '<'))
{
/**
 * Migration over module based runonce
 * 
 * Check for exists of /system/runonce
 * if not, copy the module runonce therefore
 */
$runonceJob  = 'system/modules/cm_membergooglemaps/config/RunonceJob.php';
$runonceFile = 'system/runonce.php';

if ( (file_exists(TL_ROOT . '/' . $runonceJob)) && (!file_exists(TL_ROOT . '/' . $runonceFile)) ) 
{
	//keine /system/runonce.php, let's go
	$objFile = new File($runonceJob); // hier wird intern ein "TL_ROOT/" vorgesetzt
	if ($objFile->filesize > 100) 
	{
		$objFiles = Files::getInstance();
		$objFiles->copy($runonceJob,$runonceFile);
		//
		$objFile->write("<?php // Module Migration Complete ?".">");  // Datei muss kleiner 100 Zeichen werden
	}
	$objFile->close();
}
}


?>