<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package Cm_membergooglemaps
 * @link    http://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'ChrMue',
));  


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Models
	'ChrMue\cm_MemberGoogleMaps\MemberGoogleMapsTagModel'         => 'system/modules/cm_membergooglemaps/models/MemberGoogleMapsTagModel.php',
	'ChrMue\cm_MemberGoogleMaps\MemberGoogleMapsMemberModel'      => 'system/modules/cm_membergooglemaps/models/MemberGoogleMapsMemberModel.php',
	'ChrMue\cm_MemberGoogleMaps\MemberGoogleMapsMemberGroupModel' => 'system/modules/cm_membergooglemaps/models/MemberGoogleMapsMemberGroupModel.php',
	// Modules
	'ChrMue\cm_MemberGoogleMaps\ModuleMemberGoogleMapsPos'        => 'system/modules/cm_membergooglemaps/modules/ModuleMemberGoogleMapsPos.php',
	'ChrMue\cm_MemberGoogleMaps\ModuleMemberGoogleMapsTags'       => 'system/modules/cm_membergooglemaps/modules/ModuleMemberGoogleMapsTags.php',
	'ChrMue\cm_MemberGoogleMaps\ModuleMemberFinder'               => 'system/modules/cm_membergooglemaps/modules/ModuleMemberFinder.php',
    'ChrMue\cm_MemberGoogleMaps\ModuleMemberGoogleMapsList'       => 'system/modules/cm_membergooglemaps/modules/ModuleMemberGoogleMapsList.php',
    'ChrMue\cm_MemberGoogleMaps\ModuleMemberGoogleMapsReader'	  => 'system/modules/cm_membergooglemaps/modules/ModuleMemberGoogleMapsReader.php',
	// Classes
	'ChrMue\cm_MemberGoogleMaps\UpgradeHandler'                   => 'system/modules/cm_membergooglemaps/classes/UpgradeHandler.php', 

//  'ChrMue\cm_MemberGoogleMaps\tl_cm_memberlist'  => 'system/modules/cm_membergooglemaps/classes/tl_cm_memberlist.php',

    'ChrMue\cm_MemberGoogleMaps\cm_SortWizard'                    => 'system/modules/cm_membergooglemaps/classes/cm_SortWizard.php',
    'ChrMue\cm_MemberGoogleMaps\cm_ListWizard'                    => 'system/modules/cm_membergooglemaps/classes/cm_ListWizard.php',
//	'ChrMue\cm_MemberGoogleMaps\cm_map_google_lib' => 'system/modules/cm_membergooglemaps/classes/cm_map_google_lib.php',
//  	'ChrMue\cm_MemberGoogleMaps\cm_GoogleMap_lib'                   => 'system/modules/cm_membergooglemaps/classes/cm_GoogleMap_lib.php',
  	'ChrMue\cm_MemberGoogleMaps\memberGM'                         => 'system/modules/cm_membergooglemaps/classes/memberGM.php'
));

//ChrMue\cm_MemberGoogleMaps\
 if (false && file_exists(TL_ROOT . '/system/modules/cm__lib/assets/base.php'))
 {
   include_once(TL_ROOT . '/system/modules/cm__lib/assets/base.php');
 }
 else
 {
  ClassLoader::addClasses(array
  (
  	'ChrMue\cm_MemberGoogleMaps\cm_mgm_fSockOpenConnect' => 'system/modules/cm_membergooglemaps/classes/base.php',
  	'ChrMue\cm_MemberGoogleMaps\cm_mgm_cURLConnect'      => 'system/modules/cm_membergooglemaps/classes/base.php',
  	'ChrMue\cm_MemberGoogleMaps\GoogleMapsXMLData'       => 'system/modules/cm_membergooglemaps/classes/base.php',
  ));
}//   include_once('base.php');

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	// 'cm_googlemap_js_main'                 => 'system/modules/cm_membergooglemaps/templates',
	'cm_googlemap_js_routedef'             => 'system/modules/cm_membergooglemaps/templates',
	'info_cm_membergooglemaps'             => 'system/modules/cm_membergooglemaps/templates',
	'info_cm_membergooglemaps_list'        => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_finder'             => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_googlemaps'         => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_googlemaps_detail'  => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_googlemaps_pos'     => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_googlemaps_side'    => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_googlemaps_table'   => 'system/modules/cm_membergooglemaps/templates',
	'mod_cm_memberlist_googlemaps_tabless' => 'system/modules/cm_membergooglemaps/templates',
));

