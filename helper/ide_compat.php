<?php

/*
 * This file is part of Contao.
 *
 * (c) Leo Feyer
 *
 * @license LGPL-3.0-or-later
 */

// This file is not used in Contao. Its only purpose is to make PHP IDEs like
// Eclipse, Zend Studio or PHPStorm realize the class origins, since the dynamic
// class aliasing we are using is a bit too complex for them to understand.



namespace cm_GoogleMaps {
    class GoogleMap extends \ChrMue\cm_GoogleMaps\GoogleMap {}
    class cm_GoogleMap_lib extends \ChrMue\cm_GoogleMaps\cm_GoogleMap_lib {}
    class ClusterLayout extends \ChrMue\cm_GoogleMaps\ClusterLayout {}
}
namespace Database {
    class Updater extends \Contao\Database\Updater {}
}