<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/de/tl_member_group.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2010 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_member_group']['cm_membergooglemaps_mapview'] = "Map view";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd']['0'] = "Group specific marker icon - default";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd']['1'] = "Choose the icon for that group. If you choose none the default icon will be used.";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear']['0'] = "Group specific marker icon - near range";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear']['1'] = "Choose the icon to be used for this group as result of the radial search. If you choose none the default icon will be used.";
?>