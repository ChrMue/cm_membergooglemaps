<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/de/tl_member.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_coords'][0] = "Koordinaten der Markierung";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_coords'][1] = "Bei aktiviertem Kontrollfeld werden die Koordinaten beim Speichern automatisch ermittelt, anderenfalls können Sie die Koordinaten manuell bearbeiten.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_autocoords'][0] = "Koordinaten automatisch ermitteln";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_autocoords'][1] = "Bei aktiviertem Kontrollfeld werden die Koordinaten beim Speichern automatisch ermittelt.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_lat'][0] = "Breitengrad (Grad, dezimal)";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_lat'][1] = "Bei aktiviertem Kontrollfeld werden die Koordinaten beim Speichern automatisch ermittelt, anderenfalls können Sie die Koordinaten manuell bearbeiten.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_lng'][0] = "Längengrad (Grad, dezimal)";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_lng'][1] = "Bei aktiviertem Kontrollfeld werden die Koordinaten beim Speichern automatisch ermittelt, anderenfalls können Sie die Koordinaten manuell bearbeiten.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_requestcount'][0] = "Anzahl der Versuche zur Koordinatenermittlung";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_requestcount'][1] = "Anzahl der Google-Anfragen zur Koordinatenermittlung - wird beim Speichern des Mitglieds zurückgesetzt.";
$GLOBALS['TL_LANG']['tl_member']['cm_membergooglemaps_legend'] = "Kartendarstellung";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_allowmap'][0] = "Kartendarstellung erlauben";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_indivcenter'][0] = "Individuelles Kartenzentrum";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_indivcenter'][1] = "Aktivieren Sie das Kontrollfest um ein individuelles Kartenzentrum festzulegen.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_indivzoom'][0] = "Individuelle Kartenvergrößerung";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_indivzoom'][1] = "Aktivieren Sie das Kontrollfeld um die Kartenvergrößerung individuell festzulegen.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_center'][0] = "Kartenzentrum";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_center'][1] = "Geben Sie die Koordinaten für das individuelle Kartenzentrum fest.";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_zoom'][0] = "Kartenvergrößerung";
$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_zoom'][1] = "Legen Sie den Zoomfaktor für die Kartendarstellung fest."; 

$GLOBALS['TL_LANG']['tl_member']['alias_legend'] = "Alias";
$GLOBALS['TL_LANG']['tl_member']['alias'][0] = "Mitglied-Alias";
$GLOBALS['TL_LANG']['tl_member']['alias'][1] = "Der Mitglied-Alias ist eine eindeutige Referenz, die anstelle der numerischen Mitglied-ID aufgerufen werden kann. Wenn Sie keinen Eintrag vornehmen wird der Alias automatisch gebildet als vorname-nachname-ort. Sofern kein Ort angegeben ist, entfällt der entsprechende Teil."; 

$GLOBALS['TL_LANG']['tl_member']['updCoords'] = "Fehlende Koordinaten ergänzen";


?>