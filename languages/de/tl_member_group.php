<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/de/tl_member_group.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2010 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_member_group']['cm_membergooglemaps_mapview'] = "Kartenansicht";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd']['0'] = "Gruppenspezifisches Marker-Icon - Standard";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd']['1'] = "Wählen Sie das Icon, dass für diese Gruppe verwendet werden soll. Wenn Sie kein Icon auswählen, wird das Standardicon in der Karte gezeigt.";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear']['0'] = "Gruppenspezifisches Marker-Icon - Nahbereich";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear']['1'] = "Wählen Sie das Icon, dass für diese Gruppe als ergebnis der Umkreissuche verwendet werden soll. Wenn Sie kein Icon auswählen, wird das Standardicon in der Karte gezeigt.";
?>