<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Christian Muenster 2012 
 * @author     Christian Muenster 
 * @package    CM_MemberGoogleMaps
 * @license    LGPL 
 * @filesource
 */

/**
 * palette for tl_member_group
 */
$GLOBALS['TL_DCA']['tl_member_group']['palettes']['default'] = 
str_replace('name;', 
  'name;{cm_membergooglemaps_mapview:hide},cm_map_iconstd,'
  .'cm_map_iconnear;', 
  $GLOBALS['TL_DCA']['tl_member_group']['palettes']['default']); 
/**
 * Add fields to tl_member_group
 */
$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconstd'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd'],
	'inputType'          => 'fileTree',
  	'eval'               => array('fieldType'=>'radio',
                            'files'=>true, 'filesOnly'=>true,
                            'multiple'=>false),
	'sql'				=> (version_compare(VERSION, '3.2', '<')) ? "varchar(255) NOT NULL default ''" : "binary(16) NULL" 
);

$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconnear'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear'],
	'inputType'          => 'fileTree',
  'eval'               => array('fieldType'=>'radio',
                            'files'=>true, 'filesOnly'=>true,
                            'multiple'=>false),
	'sql'				=> (version_compare(VERSION, '3.2', '<')) ? "varchar(255) NOT NULL default ''" : "binary(16) NULL" 
);

?>