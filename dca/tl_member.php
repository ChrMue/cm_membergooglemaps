<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2016 
 * @author     Christian Muenster 
 * @package    CM_MemberGoogleMaps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
 */

/**
 * Add palettes to tl_member
 */
$GLOBALS['TL_DCA']['tl_member']['config']['ctable'] = array('tl_content'); 

$GLOBALS['TL_DCA']['tl_member']['list']['operations']['edit'] = array
(
	'label'               => &$GLOBALS['TL_LANG']['tl_member']['edit'],
	'href'                => 'table=tl_content',
	'icon'                => 'edit.gif'
);

$GLOBALS['TL_DCA']['tl_member']['list']['operations']['editheader'] = array
(
	'label'               => &$GLOBALS['TL_LANG']['tl_member']['editmeta'],
	'href'                => 'act=edit',
	'icon'                => 'header.gif'
);

$GLOBALS['TL_DCA']['tl_member']['config']['sql']['keys']['alias'] ='index';
 
$GLOBALS['TL_DCA']['tl_member']['list']['label']['label_callback']=array('tl_cm_memberlist','addGMstatus');

array_insert($GLOBALS['TL_DCA']['tl_member']['list']['label']['fields'], 1, 'cm_GMstatus');

$GLOBALS['TL_DCA']['tl_member']['list']['global_operations']['updCoords'] = array
			(
        'label'               => &$GLOBALS['TL_LANG']['tl_member']['updCoords'],
        'href'                => 'key=updCoords',
        'class'               => 'header_updCoords'
			);

$GLOBALS['TL_DCA']['tl_member']['palettes']['default']=
str_replace(';{homedir_legend',
   ';{alias_legend},alias;{homedir_legend'
,$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_member']['palettes']['default']=
str_replace(';{homedir_legend',  
   ';{cm_membergooglemaps_legend},cm_googlemaps_allowmap,'
  .'cm_googlemaps_autocoords,cm_googlemaps_requestcount,cm_googlemaps_coords,'
  .'cm_googlemaps_lat,cm_googlemaps_lng,'
  .'cm_googlemaps_indivcenter,cm_googlemaps_indivzoom;{homedir_legend'
,$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);
  
$GLOBALS['TL_DCA']['tl_member']['palettes']['__selector__'][]  = 
  'cm_googlemaps_indivcenter';
$GLOBALS['TL_DCA']['tl_member']['palettes']['__selector__'][]  = 
  'cm_googlemaps_indivzoom';

$GLOBALS['TL_DCA']['tl_member']['subpalettes']['cm_googlemaps_indivcenter'] =
     'cm_googlemaps_center';
$GLOBALS['TL_DCA']['tl_member']['subpalettes']['cm_googlemaps_indivzoom'] =
     'cm_googlemaps_zoom';


if (TL_MODE != 'BE') {
    $GLOBALS['TL_DCA']['tl_member']['config']['onsubmit_callback'][] = array('cm_MemberGoogleMaps\memberGM', 'getGMGeoDataFE');
    $GLOBALS['TL_DCA']['tl_member']['config']['onsubmit_callback'][] = array('cm_MemberGoogleMaps\memberGM', 'resetCounterFE');
}

/**
 * Add fields to tl_member
 */
$GLOBALS['TL_DCA']['tl_member']['fields']['alias'] = array
(
		'label'         => &$GLOBALS['TL_LANG']['tl_member']['alias'],
		'exclude'       => true,
		'search'        => true,
		'inputType'     => 'text',
		'eval'          => array('rgxp'=>'alias', 'unique'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
		'save_callback' => array
		(
			array('tl_cm_memberlist', 'generateAlias')
		),
		'sql'                     => "varchar(128) COLLATE utf8_bin NOT NULL default ''"
); 
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_allowmap'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_allowmap'],
	'inputType'          => 'checkbox',
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>false, 'feGroup'=>'contact'),
	'sql'				=> "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_autocoords'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_autocoords'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>false, 'feGroup'=>'contact',
                                'tl_class'=>'w50'),
	'sql'				=> "char(1) NOT NULL default '1'"
);
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_requestcount'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_requestcount'],
	'inputType'          => 'text',
	'eval'               => array('mandatory'=>false, 'feEditable'=>false, 
                                'feViewable'=>false,'rgxp'=>'natural', 
                                'readonly'=>true,'disabled'=>false,'tl_class'=>'w50'), 
	'search'             => false,
	'save_callback' => array
						(
							array('cm_MemberGoogleMaps\memberGM', 'resetCounterBE')
						),
	'sql'				=> "int(10) NOT NULL default '0'"
);
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_coords'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_coords'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50', 'maxlength'=>64,'rgxp'=>'geocoords'),
	'save_callback' => array
						(
							array('cm_MemberGoogleMaps\memberGM', 'getGMGeoDataBE')
						),
	'sql'				=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_lat'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_lat'],
	'inputType'          => 'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true,
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50 clr', 'maxlength'=>32,
                                'readonly'=>true,'disabled'=>true),
	'sql'				=> "varchar(32) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_lng'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_lng'],
	'inputType'          => 'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true,
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50', 'maxlength'=>32,
                                'readonly'=>true,'disabled'=>true ),
	'sql'				=> "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_indivcenter'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_indivcenter'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'tl_class'=>'w50 m12 clr','feViewable'=>false, 'feGroup'=>'contact',
                                'submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_center'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_center'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'maxlength'=>64,'rgxp'=>'geocoords',
                                'tl_class'=>'w50'),
	'sql'				=> "varchar(64) NOT NULL default ''"	                                
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_indivzoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_indivzoom'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'tl_class'=>'w50 m12 clr','feViewable'=>false, 'feGroup'=>'contact',
                                'submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_googlemaps_zoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_googlemaps_zoom'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50'),
	'sql'				=> "int(2) NOT NULL default '15'"	                                
);

class tl_cm_memberlist extends tl_member
{

	/**
	 * Auto-generate the meber alias if it has not been set yet
	 * @param mixed
	 * @param \DataContainer
	 * @return string
	 * @throws \Exception
	 */
	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;

		// Generate alias if there is none
		if ($varValue == '')
		{
			$autoAlias = true;
			$alias = sprintf('%s-%s',$dc->activeRecord->firstname,$dc->activeRecord->lastname);
			if ($dc->activeRecord->city)
			{
			 $alias .='-'.$dc->activeRecord->city;
			}
			if (!version_compare(VERSION, '3.5', '<'))
			{
				$varValue = standardize(StringUtil::restoreBasicEntities($alias));
			}
			else {
				$varValue = standardize(String::restoreBasicEntities($alias));
			}
		}

		$objAlias = $this->Database->prepare("SELECT id FROM tl_member WHERE alias=?")
								   ->execute($varValue);

		// Check whether the news alias exists
		if ($objAlias->numRows > 1 && !$autoAlias)
		{
			throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
		}

		// Add ID to alias
		if ($objAlias->numRows && $autoAlias)
		{
			$varValue .= '-' . $dc->id;
		}

		return $varValue;
	} 

	/**
	 * Return all editable fields of table tl_member
	 * @return array
	 */

	public function getViewableMemberProperties()
	{
		$return = array();

		$this->loadLanguageFile('tl_member');
		$this->loadDataContainer('tl_member');

		foreach ($GLOBALS['TL_DCA']['tl_member']['fields'] as $k=>$v)
		{
			if ($k == 'username' || $k == 'password' || $k == 'newsletter' 
            || $k == 'publicFields' || $k == 'allowEmail')
			{
				continue;
			}

			if ($v['eval']['feViewable'])
			{
				$return[$k] = $GLOBALS['TL_DCA']['tl_member']['fields'][$k]['label'][0];
			}
		}

		return $return;
	}


  /**
   * use hook createNewUser to add google map coordinates
   */
  public function createNewUser($intId, $arrData,$mod)
  {

	if ($mod instanceof Contao\ModuleRegistration)
	{
		$arr=array();
		if ($mod->cm_register_autocoord)
		{
        	$arr['cm_googlemaps_autocoords']=1;
            $arrData['cm_googlemaps_autocoords']=1;
		}
		if ($mod->cm_register_showonmap)
		{
        	$arr['cm_googlemaps_allowmap']=1;
            $arrData['cm_googlemaps_allowmap']=1;
		}
        if (count($arr)>0)
        	$this->Database->prepare("UPDATE tl_member %s WHERE id=?")->set($arr)->execute($intId);
	}
    $arrData['id']= $intId;
    // $key = \config::get('cm_map_apikey');
    // $coords = ChrMue\cm_GoogleMaps\HelperGM::getGMGeoData($intId,(object)$arrData,$arrData->cm_googlemaps_coords,$key);
    $helper = new ChrMue\cm_GoogleMaps\HelperGM();
    $coords= $helper->getGMGeoDataFE($intId,$arrData);
  }

  private function validateCoords($coords) {
    return !($coords=="," || $coords=="");
  }

/*  
  public function listMember($row, $label, \DataContainer $xx,$htmlContent=null) {
  if (!$row["cm_googlemaps_autocoords"])
    $img= "flag_yellow";
   else
    $img=$this->validateCoords($row["cm_googlemaps_coords"])?"flag_green":"flag_red";
                                      
   $imghtml='<img class="coords" src="system/modules/cm_membergooglemaps/html/'.$img.'.png" alt="" /> ';

    if ($htmlContent==null)
      return parent::addIcon($row, $imghtml.$label);
    else
    {
      $ic=parent::addIcon($row, $imghtml.$label,$xx,$htmlContent);

      $ic[0]='<div style="float:right;padding-left:10px;">'.$imghtml.'</div>'.$ic[0];
      return $ic;
    }
  }
*/

  public function addGMstatus($row, $label, \DataContainer $dc, $args) {
      
    $args = $this->addIcon($row, $label, $dc, $args);
  
    $flags = array (
        'indiv'    => 'flag_yellow',
        'ok'       => 'flag_green',
        'failed'   => 'flag_red',
        'limited'  => 'flag_limit'
    );

        
    $img='';
    $alt='';
    $limit = \Config::get('cm_requestlimit');
    if (!$limit) $limit=3;
   
    if (!$row["cm_googlemaps_autocoords"])
    {
        $status= "indiv";
    }
    elseif ($this->validateCoords($row["cm_googlemaps_coords"]))
    {
        $status = "ok";
    }
    else
    {
        $status = ($row["cm_googlemaps_requestcount"]>$limit)? "limited":"failed";
    }
    $alt=$status;
    $img=$flags[$status];
                                        
    $imghtml='<div><img class="coords" src="system/modules/cm_googlemaps/assets/'.$img.'.png" alt="'.$img.'" /></div>';

    $args[1] = $imghtml;
    return $args;
  }

}


?>