<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2010
 * @author     Christian Muenster 
 * @package    Controller
 * @license    LGPL 
 * @filesource
 */

/**
 * Class cm_SortWizard
 *
 * @copyright  Christian Muenster 2010
 * @author     Christian Muenster 
 * @package    cm_MemberGoogleMaps 
 *
 * based on originally:
 * Class OptionWizard
 * @copyright  Leo Feyer 2005
 * @author     Leo Feyer <leo@typolight.org>
 * @package    Controller
 */
namespace ChrMue\cm_MemberGoogleMaps;


class cm_ListWizard extends \Widget
{

	/**
	 * Submit user input
	 * @var boolean
	 */
	protected $blnSubmitInput = true;

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'be_widget';


	/**
	 * Add specific attributes
	 * @param string
	 * @param mixed
	 */
	public function __set($strKey, $varValue)
	{
		switch ($strKey)
		{
			case 'value':
				$this->varValue = deserialize($varValue);
				break;

			case 'mandatory':
				$this->arrConfiguration['mandatory'] = $varValue ? true : false;
				break;


			default:
				parent::__set($strKey, $varValue);
				break;
		}
	}


	/**
	 * Validate input and set value
	 * /
	public function validate()
	{
		$mandatory = $this->mandatory;
		$options = $this->getPost($this->strName);

		// Check labels only (values can be empty)
		if (is_array($options))
		{
			foreach ($options as $key=>$option)
			{
				$options[$key]['field'] = trim($option['field']);

				if (strlen($options[$key]['key']))
				{
					$this->mandatory = false;
				}
			}
		}

		$varInput = $this->validator($options);

		if (!$this->hasErrors())
		{
			$this->varValue = $varInput;
		}

		// Reset the property
		if ($mandatory)
		{
			$this->mandatory = true;
		}
	}


	/ **
	 * Generate the widget and return it as string
	 * @return string
	 */
	public function generate()
	{
			$this->import('Database');

		$arrButtons = array('copy', 'up', 'down', 'delete');
		$strCommand = 'cmd_' . $this->strField;

		// Change the order
		if (\Input::get($strCommand) && is_numeric(\Input::get('cid')) && \Input::get('id') == $this->currentRecord)
		{

			switch (\Input::get($strCommand))
			{
				case 'copy':
					$this->varValue = array_duplicate($this->varValue, \Input::get('cid'));
					break;

				case 'up':
					$this->varValue = array_move_up($this->varValue, \Input::get('cid'));
					break;

				case 'down':
					$this->varValue = array_move_down($this->varValue, \Input::get('cid'));
					break;

				case 'delete':
					$this->varValue = array_delete($this->varValue, \Input::get('cid'));
					break;
			}
		}
		if (\Input::get($strCommand) || \Input::post('FORM_SUBMIT') == $this->strTable)
		{
			$this->Database->prepare("UPDATE " . $this->strTable . " SET " . $this->strField . "=? WHERE id=?")
						   ->execute(serialize($this->varValue), $this->currentRecord);

			if (is_numeric(\Input::get('cid')) && \Input::get('id') == $this->currentRecord)
			{
			$this->redirect(preg_replace('/&(amp;)?cid=[^&]*/i', '', preg_replace('/&(amp;)?' . preg_quote($strCommand, '/') . '=[^&]*/i', '', \Environment::get('request'))));
		}
		}
		// Make sure there is at least an empty array
		if (!is_array($this->varValue) || !$this->varValue[0])
		{
			$this->varValue = array(array(''));
		}

		// Begin table
		$return .= '<table cellspacing="0" cellpadding="0" class="tl_modulewizard" id="ctrl_'.$this->strId.'" summary="Field wizard">
  <thead>
    <tr>
      <th>'.$GLOBALS['TL_LANG']['tl_module']['cm_field'].'</th><th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>';
		// Add fields
		for ($i=0; $i<count($this->varValue); $i++)
		
		{
			$return .= '
    <tr>
      <td><select name="'.$this->strId.'['.$i.'][field]" id="'.$this->strId.'_field_'.$i.'" class="tl_select tl_chosen" onfocus="Backend.getScrollOffset();">
      ';
      foreach ($this->options as $memberfield) 
      {
			  $return .= '
        <option value="'.$memberfield['value'].'" '
        .(($this->varValue[$i]['field'] == $memberfield['value'])?'selected="selected"':'').'>'.$memberfield['label'].'</option> 
        ';
      }
			$return .= '
      </select>
      </td>
      ';
	
			// Add row buttons
			$return .= '
      <td style="white-space:nowrap; padding-left:3px;">';

			foreach ($arrButtons as $button)
			{
				$return .= '<a href="'.$this->addToUrl('&'.$strCommand.'='.$button.'&cid='.$i.'&id='.$this->currentRecord).'" title="'.specialchars($GLOBALS['TL_LANG'][$this->strTable]['cm_memberlist_'.$button][0]).'" onclick="Backend.moduleWizard(this, \''.$button.'\', \'ctrl_'.$this->strId.'\'); return false;">'.$this->generateImage($button.'.gif', $GLOBALS['TL_LANG'][$this->strTable]['cm_memberlist_'.$button][0], 'class="tl_listwizard_img"').'</a> ';
			}

			$return .= '</td>
    </tr>';
		}

		return $return.'
  </tbody>
  </table>';
	}
}

?>