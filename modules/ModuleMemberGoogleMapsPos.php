<?php

/**
 * cm_MemberGoogleMaps
 * @copyright  Christian Muenster 2009-2018
 * @author     Christian Muenster
 * @package    cm_membergooglemaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberGoogleMaps;


/**
 * Class ModuleMemberGoogleMapsPos
 *
 * @copyright  Christian Muenster 2017
 * @author     Christian Muenster 
 * @package    Controller
 */
class ModuleMemberGoogleMapsPos extends \Module
{


	/**
	 * Template
	 * @var string
	 */
protected $strTemplate = 'mod_cm_memberlist_googlemaps_pos';
	/**
	 * Groups
	 * @var array
	 */
protected $arrMlGroups = array();


protected $mappos = 'below';
	/**
	 * Fields
	 * @var array
	 */
protected $arrMlFields = array();

private $useSSL = false;


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### Show MEMBER with Google Map V3 ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{
		$this->useSSL=\Config::get('cm_request_gm_ssl') || \Environment::get('ssl');
    $this->showMember($this->cm_membergooglemaps_member);

		//parent::compile();

	}
	
	/**
	 * List a single member
	 * @param integer
	 */
	protected function showMember($id)
	{
		global $objPage;

		$time = time();
		$this->Template->record = array();

		// Get member
		$objMember = \Database::getInstance()->prepare("SELECT * FROM tl_member WHERE id=? AND disable!=1 AND (start='' OR start<=?) AND (stop='' OR stop>=?)")
									->limit(1)
									->execute($id, $time, $time);

		// No member found or group not allowed
/*
		echo $objMember->numRows.+"<br />"
        .$objMember->firstname."&nbsp;"
        .$objMember->lastname."<br />"
        .$objMember->street."<br />";
*/
		if ($objMember->numRows < 1)
		{
  			$this->Template->invalid = $GLOBALS['TL_LANG']['MSC']['invalidUserId'];

			// Do not index the page
			$objPage->noSearch = 1;
			$objPage->cache = 0;

			// Send 404 header
			header('HTTP/1.1 404 Not Found');
			return;
		}
		if (($objMember -> website))
		{
			if (preg_match('@^(https://)@i', $objMember -> website))	
			{
				$objMember -> website = substr($objMember -> website,8);
			}
			elseif (preg_match('@^(http://)@i', $objMember -> website)) {
				$objMember -> website = substr($objMember -> website,7);
			}
			
		}	
		

		// Default variables
		$this->Template->action = ampersand($this->Environment->request);
		$this->Template->referer = 'javascript:history.go(-1)';
		$this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];
		$this->Template->publicProfile = sprintf($GLOBALS['TL_LANG']['MSC']['publicProfile'], $objMember->username);
		$this->Template->noPublicInfo = $GLOBALS['TL_LANG']['MSC']['noPublicInfo'];
		$this->Template->sendEmail = $GLOBALS['TL_LANG']['MSC']['sendEmail'];
		$this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
		$this->Template->loginToSend = $GLOBALS['TL_LANG']['MSC']['loginToSend'];
		$this->Template->emailDisabled = $GLOBALS['TL_LANG']['MSC']['emailDisabled'];

		// Confirmation message
		if ($_SESSION['TL_EMAIL_SENT'])
		{
			$this->Template->confirm = $GLOBALS['TL_LANG']['MSC']['messageSent'];
			$_SESSION['TL_EMAIL_SENT'] = false;
		}

		// Check personal message settings
		switch ($objMember->allowEmail)
		{
			case 'email_all':
				$this->Template->allowEmail = 3;
				break;

			case 'email_member':
				$this->Template->allowEmail = FE_USER_LOGGED_IN ? 3 : 2;
				break;

			default:
				$this->Template->allowEmail = 1;
				break;
		}

		// No e-mail address given
		if (!$objMember->email && !strlen($objMember->email))
		{
			$this->Template->allowEmail = 1;
		}
		else
		{
//		  $value = $this->String->encodeEmail($objMember->email);
//			$value = '<a href="mailto:' . $value . '">' . $value . '</a>';
//			$this->Template->emailtext = $value;
        }

		// Handle personal messages
		if ($this->Template->allowEmail > 1)
		{
			$arrField = array
			(
				'name'      => 'message',
				'label'     => $GLOBALS['TL_LANG']['MSC']['message'],
				'inputType' => 'textarea',
				'eval'      => array('mandatory'=>true, 'required'=>true, 'rows'=>4, 'cols'=>40, 'decodeEntities'=>true)
			);

			$arrWidget = \Widget::getAttributesFromDca($arrField, $arrField['name'], '');
			$objWidget = new \FormTextArea($arrWidget);

			// Validate widget
			if (\Input::post('FORM_SUBMIT') == 'tl_send_email')
			{
				$objWidget->validate();

				if (!$objWidget->hasErrors())
				{
					$this->sendPersonalMessage($objMember, $objWidget);
				}
			}

			$this->Template->widget = $objWidget;
			$this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
		}
	    $mapID  = "cm_map".$this->mappos."_".$this->id;
		$arrFields = deserialize($objMember->publicFields);
		
		$hasGoogleMaps=preg_match('/^[-]{0,1}\d+(\.\d*){0,1},[-]{0,1}\d+(\.\d*){0,1}$/',
		                          $objMember->cm_googlemaps_coords);
		$this->Template->hasGoogleMaps=$hasGoogleMaps;
		if ($hasGoogleMaps)
        {
            $center = $this->cm_map_indivcenterpos ? $this->cm_map_centerpos : $objMember->cm_googlemaps_coords;

            $root_id = \Frontend::getRootPageFromUrl()->id; 
            //$root_details = $this->getPageDetails($root_id);
            $root_details = \PageModel::findWithDetails($root_id);
            
            $map_apikey = $root_details->cm_map_apikey;
            if (!$map_apikey) $map_apikey=\Config::get('cm_map_apikey');
            
            $baseJsScript =  \cm_GoogleMaps\cm_GoogleMap_lib::getBaseScript($this->useSSL,$language,$map_apikey);
            $acceptanceRequired=false;
            $acceptanceText='';
            switch($this->cm_gm_acceptance_required)
            {
                case 'on':  $acceptanceRequired = true;
                $acceptanceText=$this->cm_gm_acceptance_text;
                break;
                case 'off':   $acceptanceRequired = false;
                $acceptanceText="";
                break;
                case 'page':
                default:    $acceptanceRequired = $root_details->cm_gm_acceptance_required??false;
                $acceptanceText = $root_details->cm_gm_acceptance_required?$root_details->cm_gm_acceptance_text:'';
                break;
            }
            $this->Template->BaseScriptCode= $baseJsScript;
            $this->Template->GoogleMapCode = $this->getGoogleMapCode($objMember->cm_googlemaps_coords,$center,$this->cm_map_zoompos,$mapID);
            $this->Template->acceptanceRequired = $acceptanceRequired;
            $this->Template->acceptanceText =$acceptanceText;
            
        }
        else
        {
            $this->log("Koord. \"".$objMember->cm_googlemaps_coords."\"nicht korrekt (".$objMember->id.")","cm_membergooglemaps",TL_GENERAL);
            //$this->Template->GoogleMapCode = $GLOBALS['TL_LANG']['MSC']['cm_membergooglemaps_error'];
            $this->Template->GoogleMapCode = '';
            $this->Template->acceptanceRequired=false;
            $this->Template->acceptanceText ="";
        }
        
        if ($this->cm_map_heightdetail) 
        {
            $heightArr=deserialize($this->cm_map_heightdetail);
            if ($heightArr["value"])
            {
                $this->Template->mapstyle="height:".$heightArr["value"].$heightArr["unit"];
            }
        }
	  $this->Template->mappos = $this->mappos;
	  $this->Template->mapID  = $mapID;
  
  }

  protected function getGoogleMapCode($coordinates,$center,$zoom,$mapID)
  {
    $language = 'de';

    $mapType = \cm_GoogleMaps\cm_GoogleMap_lib::getMapTypeStr($this->cm_map_maptypedetail);
//    $showTypePanel = $this->cm_map_choosetypedetail;
    $infoText = preg_replace("/\r|\n/s", "", $this->cm_map_infotextdetail);
    if ($this->cm_map_routetodetail)
    {
      $infoText .= ($infoText?'<p class="route">':'').$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'].': ';
    }
//    if ($showTypePanel)
//      $typePanel='map.addControl(new GMapTypeControl());';


//    $root_id=\Frontend::getRootPageFromUrl()->id;
//    $root_details =  \PageModel::findWithDetails($root_id);
    
//    $map_apikey = $root_details->cm_map_apikey;
//    if (!$map_apikey) $map_apikey=\Config::get('cm_map_apikey');
        

//    $baseJsScript = \cm_GoogleMaps\cm_GoogleMap_lib::getBaseScript($this->useSSL,false, $language,$map_apikey);
//    $code =$baseJsScript.'<script type="text/javascript">
//    /* <![CDATA[ */
//    '. "\n";

//--------------------------------------------
   $code .= ' 
    var infoText=\''.$infoText.'\';
    var infowindow;
    var '.$mapID.'to_html="";
    var '.$mapID.'from_html="";
    var '.$mapID.'myMarker;
    var mappos = new google.maps.LatLng('.$coordinates.');';

    if ($this->cm_map_routetodetail)
    {
      $bubbleRoute =
      $mapID.'to_html = infoText + \'<b>'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
             .'<\/b> - <a href="javascript:'.$mapID.'fromlocation()">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
             .'<\/a>\' + ' . "\n"
//             .'\'<br \/>'
             .'\'<\/p><p class="routefrom">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_fromaddr']
             .':<form class="route" action="'.($this->useSSL?'https://':'http://').'maps.google.com/maps" method="get" target="_blank">\' + ' . "\n"
             .'\'<input type="text" SIZE=23 MAXLENGTH=50 name="saddr" id="saddr" value="" /> \' + ' . "\n"
             .'\'<INPUT value="Los" TYPE="SUBMIT">\' + ' . "\n"
             .'\'<input type="hidden" name="daddr" value="\' + mappos.lat() + \',\' + mappos.lng() + ' . "\n" 
             .'\'"/><\/form><\/p>\';';
      $bubbleRoute .=
      $mapID.'from_html = infoText + \'<a href="javascript:'.$mapID.'tolocation()">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
             .'<\/a> - <b>'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
             .'<\/b>\' + ' . "\n"
             .'\'<\/p><p class="routeto">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_toaddr']
             .':<form class="route" action="'.($this->useSSL?'https://':'http://').'maps.google.com/maps" method="get"" target="_blank">\' + ' . "\n"
             .'\'<input type="text" SIZE=23 MAXLENGTH=50 name="daddr" id="daddr" value="" /> \' + ' . "\n"
             .'\'<INPUT value="Los" TYPE="SUBMIT">\' + ' . "\n"
             .'\'<input type="hidden" name="saddr" value="\' + mappos.lat() + \',\' + mappos.lng() + ' . "\n"
             .'\'"/><\/form><\/p>\';';
      $bubbleContent = "\n" . $bubbleRoute . "\n";
    }
   $code .= ($this->cm_map_routetodetail ? $bubbleContent : "")."\n";


      if ($this->cm_map_routetodetail)
      {
        $code .='function '.$mapID.'tolocation() {
                  infowindow.setContent('.$mapID.'to_html);
                }
                function '.$mapID.'fromlocation() {
                  infowindow.setContent('.$mapID.'from_html);
                }'."\n";
      }
//--------------------------------------------
      $code .='function '.$mapID.'_initialize() {
      if(!window.google){
          return false;
      }'."\n";
      $code .='mappos = new google.maps.LatLng('.$coordinates.');';
    if ($this->cm_map_routetodetail)
    {
      $code .='infoText= infoText + \'<a href="javascript:'.$mapID.'tolocation()">'
      .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
      .'<\/a> - <a href="javascript:'.$mapID.'fromlocation()">'
      .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
      .'<\/a><\/p>\';';
    }
//--------------------------------------------
    
    $code .='var mapOptions = {
        center: new google.maps.LatLng('.$center.'),
        mapTypeControl: '.($this->cm_map_choosetypedetail?'true':'false').',';
        if ($this->cm_map_choosetypedetail && ($this->cm_map_ctrltypedetail!='')) {
          $code .='mapTypeControlOptions: {
           style: google.maps.MapTypeControlStyle.';
          $code .= \cm_GoogleMaps\cm_GoogleMap_lib::getCtrlTypeStr($this->cm_map_ctrltypedetail);
          $code .='},';
        }
        $code .='navigationControl: '.($this->cm_map_choosenavdetail?'true':'false').',';
        if ($this->cm_map_choosenavdetail && ($this->cm_map_ctrlnavdetail!='')) {
          $code .='navigationControlOptions: {
           style: google.maps.NavigationControlStyle.';
          $code.= \cm_GoogleMaps\cm_GoogleMap_lib::getCtrlNavStr($this->cm_map_ctrlnavdetail);
          $code .='},';
        }
        $code.='mapTypeId: google.maps.MapTypeId.'.$mapType.',
        zoom: '.$zoom.' 
      }
      
      var map = new google.maps.Map(document.getElementById("'.$mapID.'"),mapOptions);'."\n";
    
//--------------------------------------------
      $code .=
      $mapID.'myMarker = new google.maps.Marker({
      position: mappos, 
      map: map'; 
      if ($iconStd)
        $code .=', icon: "'.$iconStd.'"';
      $code .='});

      infowindow = new google.maps.InfoWindow({
          content: infoText
      });'."\n";     
      

       $code .='google.maps.event.addListener('.$mapID.'myMarker, "click", function() {
          infowindow.open(map,'.$mapID.'myMarker);
        });';
      if ($this->cm_map_infoShowOnload)
      {
        $code.='infowindow.open(map,'.$mapID.'myMarker);';
      }
     $code .='}';

    $code .="
     if(window.addEvent) {
         window.addEvent('domready', function() {
             ".$mapID."_initialize();
    });
    } else if(typeof jQuery == 'function') {
        jQuery(document).ready(function(){
            ".$mapID."_initialize();
        });
    } else {
        window.setTimeout('".$mapID."_initialize()', 500);
    }";
    return $code;
  }

}

?>